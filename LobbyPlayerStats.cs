﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LobbyPlayerStats : NetworkBehaviour {
	[SyncVar]
	public int myNetId;
	public float sessionScore;
	void Start(){
		//initialize netId
		myNetId = int.Parse (GetComponent<NetworkIdentity> ().netId.ToString ());
		//turn off the local player canvas
		if (isLocalPlayer) {
			GetComponentInChildren<Canvas> ().enabled = false;
		}
	}
}
