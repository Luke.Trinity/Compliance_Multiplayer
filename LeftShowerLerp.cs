﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftShowerLerp : MonoBehaviour
{
	//booleans to trigger an open or close of this door
	public static bool openingR = false;
	public static bool closingR = false;
	//range of values to interpolate
	private float maximum = 1.021f;
	private float minimum =  0f;
	// starting value for the Lerp interpolater
	public static float t = 0.0f;

	void Update ()
	{		
		//if opening or closing the door set the size delta y value of the rect transform using the lerp function
		if (openingR) {
			GetComponent<RectTransform> ().sizeDelta = new Vector2 (.07f, Mathf.Lerp (GetComponent<RectTransform> ().sizeDelta.y, minimum, t));
			// .. and increate the t interpolater
			t += 0.5f * Time.deltaTime;
			if (GetComponent<RectTransform> ().sizeDelta == new Vector2 (.07f, minimum)) {
				t = 0f;
				openingR = false;
			}
		} else if(closingR) {
			GetComponent<RectTransform> ().sizeDelta = new Vector2 (.07f, Mathf.Lerp (GetComponent<RectTransform> ().sizeDelta.y, maximum, t));
			// .. and increate the t interpolater
			t += 0.5f * Time.deltaTime;
			if (GetComponent<RectTransform> ().sizeDelta == new Vector2 (.07f, maximum)) {
				t = 0f;
				closingR = false;
			}
		}
	}
}
