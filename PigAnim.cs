﻿using UnityEngine;
using System.Collections;

//this script controls basic pig animations based on a timer
public class PigAnim : MonoBehaviour {
	//routine for pig animation
	private IEnumerator pigAnimRoutine;
	//speed passed into routine
	private float pigAnimSpeed;
	//string [][] of tags
	private string[][] tags = new string[][] {
		new string[] {"pig3Parent","SecondTimerParent1","sidePig1"},new string[] {"pig2Parent","SecondTimerParent2","sidePig2"},
		new string[] {"pig2Parent","SecondTimerParent2","sidePig2"},new string[] {"pig1Parent","SecondTimerParent3","sidePig3"},
		new string[] { "pig1Parent", "SecondTimerParent3","sidePig3"},new string[] { "pig2Parent","SecondTimerParent2","sidePig4"},
		new string[] { "pig2Parent", "SecondTimerParent2","sidePig4"},new string[] { "pig3Parent","SecondTimerParent1","sidePig3"},
		new string[] { "pig3Parent", "SecondTimerParent1","sidePig3"},new string[] { "pig2Parent","SecondTimerParent2","sidePig2"},
		new string[] { "pig2Parent", "SecondTimerParent2","sidePig2"},new string[] { "pig3Parent","SecondTimerParent1","sidePig1"}
	};
	//startIndex for tags
	private int tagIndex;
	//initialization
	void Start(){
		tagIndex = 0;
		pigAnimSpeed = .5f;
		//define the routine for spawning tasks
		pigAnimRoutine = PigAnimation (pigAnimSpeed);
		//start the routine
		StartCoroutine (pigAnimRoutine);
	}

	//IEnumerator to animate pigs throughout game
	private IEnumerator PigAnimation (float pigAnimSpeed)
	{
		//continue doing this indefinitely
		while (true) {
			//wait to execute the block
			yield return new WaitForSeconds (pigAnimSpeed);
			//turn off the child renderers for the first tags
			foreach(string tag in tags[tagIndex%tags.Length]){
				GameController.SetChildRenderers(tag, false);
			}
			tagIndex++;
			//turn on the child renderers for the next tag array
			foreach (string tag in tags[tagIndex%tags.Length]){
				GameController.SetChildRenderers(tag, true);
			}
			tagIndex++;
		}
	}
}
