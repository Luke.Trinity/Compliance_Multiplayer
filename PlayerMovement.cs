using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMovement : NetworkBehaviour
{
	//map from tiled
	public GameObject gamePrefab;
	//local player camera
	public GameObject cameraPrefab;
	//text for round end information scene
	public GameObject playerInfoPrefab;
	//location to instantiate prefabs
	public Transform gameSpawn;
	//names of animation controllers in resources folder
	private string[] animatorPaths = {
		"FourthOptionDirtyAC",
		"FourthOptionCleanAC",
		"ThirdOptionDirtyAC",
		"ThirdOptionCleanAC",
		"SecondOptionDirtyAC",
		"SecondOptionCleanAC",
		"BasicDirtyAC",
		"BasicCleanAC",
	};
	//coin object for internal tasks
	public GameObject coin;
	//controls movement and collisions
	private Rigidbody2D rigbod;
	//player animator
	private Animator anim;
	//true when the the player in the barn
	public bool inBarn;
	//local score
	public float playerScore;
	//routine to increment score while on outdoor task area
	private IEnumerator countUpRoutine;
	//this is a local variable related to task instantiation
	public bool taskExitColorsChanged;
	//for local/nonlocal shower behavior i.e. Showering countdown only for player actuall showering
	public bool showerFreezeEnter;
	//local var to reset collect coin text
	public bool reenterText;
	//keep track of showerTime
	private int freezeTimer;
	//index for color array during shower countdown
	private int colorArrayIndex;
	//load level count down
	private IEnumerator loadLevelRoutine;
	//wait to enable player visibility
	private IEnumerator unHideRoutine;
	//shower freeze count down
	private IEnumerator countDownRoutine;
	//instantiate coins
	public IEnumerator taskSpawnRoutine;
	//ienumerator to flash local score increment
	private IEnumerator scoreFlashRoutine;
	//ienumerator to count down and reload next round
	private IEnumerator waitForNextRound;
	//array coin spawn positions
	private Vector2[] spawnPositions;
	//index for spawn position
	private int spawnIndex;
	//coins currently spawned
	private int numCoinsSpawned;
	//arrays of positions by player
	private Vector2[] player1CoinSpawnPositions = new Vector2[] {
		new Vector2 (4.5f, -7f),
		new Vector2 (7.3f, -8.2f),
	};
	private Vector2[] player2CoinSpawnPositions = new Vector2[] {
		new Vector2 (4.7f, -7.4f),
		new Vector2 (7.5f, -7.8f),
	};
	private Vector2[] player3CoinSpawnPositions = new Vector2[] { 
		new Vector2 (4.9f, -7.8f),
		new Vector2 (7.7f, -7.4f)
	};
	private Vector2[] player4CoinSpawnPositions = new Vector2[] { 		
		new Vector2 (5.1f, -8.2f),
		new Vector2 (7.9f, -7f)
	};
	//network identity number
	[SyncVar]
	public int myNetId;
	//left/right direction during shower force walk
	private int leftRight;
	//client update to server of animator
	private bool localInitialization;
	//track if we have called the create game method
	public static bool gameCreated;
	//color of local player coin
	private Color myColor;
	//if this player infected the pigs
	public bool infectionIncurred;
	//this is the variable that seals the barn if the player does not exit
	public bool rpcNeeded;
	//this helps avoid a false seal-out if the player did not actually exit
	private bool allowClosal;
	//to control change of clothes for shower exit
	private int animIndex;
	//if we need to load the next scene at the end of the day
	public bool endDayUpdate;
	//store the players round movements
	private static LinkedList<inputState> myGameplay = new LinkedList<inputState>();
	//enumerator for update calls
	private int updateCalls;
	//movement vector for botification
	private Vector2 tempMoveVect;

	//Called on initialization
	void Start ()
	{
		updateCalls = 0;
		taskExitColorsChanged = false;
		//true when player enters the right shower exit when not in the barn, need to implement for fire exit
		allowClosal = false;
		infectionIncurred = false;
		gameCreated = false;
		localInitialization = false;
		leftRight = -1;
		inBarn = true;
		playerScore = 0f;
		freezeTimer = 0;
		colorArrayIndex = 0;
		numCoinsSpawned = 0;
		spawnIndex = 0;
		//private local vars to control locality i.e. only one player sees doors opening
		showerFreezeEnter = false;
		rpcNeeded = false;
		endDayUpdate = false;
		reenterText = false;
		//set up each camera to follow the local player
		if (isLocalPlayer) {
			CameraFollow.target = this.GetComponentInParent<Transform> ();
			this.transform.GetChild (1).GetComponentInChildren<Text> ().text = "My Worker";
		}
		//assign each player's local rb and animator
		rigbod = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		//assign spawn positions, local player coin color, player name, and animator
		switch (myNetId) {
		case(1):
			animIndex = 7;
			spawnPositions = player1CoinSpawnPositions;
			myColor = Color.red;
			break;
		case(2):
			animIndex = 5;
			spawnPositions = player2CoinSpawnPositions;
			myColor = Color.cyan;
			break;
		case(3):
			animIndex = 3;
			spawnPositions = player3CoinSpawnPositions;
			myColor = Color.yellow;
			break;
		case(4):
			animIndex = 1;
			spawnPositions = player4CoinSpawnPositions;
			myColor = Color.green;
			break;
		}
		//load animation controller
		anim.runtimeAnimatorController = Resources.Load (animatorPaths [animIndex]) as RuntimeAnimatorController;
		//define the routine for spawning tasks
		taskSpawnRoutine = SpawnTasks (myNetId, myColor);
		//start the routine
		StartCoroutine (taskSpawnRoutine);
		//ignore collisions with all other players
		foreach (GameObject p in GameObject.FindGameObjectsWithTag ("Player")) {
			Physics2D.IgnoreCollision (GetComponent<CircleCollider2D> (), p.GetComponent<CircleCollider2D> ());
		}
		GameController.playerInitializations++;
		if (GameController.playerInitializations < GameController.totalPlayers) {
			//disable the player movement script if this is not the last player to spawn
			GetComponentInChildren<PlayerMovement> ().enabled = false;
		} else {
			creator ();
		}
	}
	//this method will be called when the game is ready to be created
	private void creator ()
	{
		//enable the player movement script for all players
		foreach (GameObject p in GameObject.FindGameObjectsWithTag("Player")) {
			p.GetComponentInChildren<PlayerMovement> ().enabled = true;
		}
		OutdoorTaskController.numTasksInitialized = 0;
		//set up players array
		GameController.players = GameObject.FindGameObjectsWithTag ("Player");
		//remove the existing camera needed for refresh of network manager hud
		Destroy (GameObject.Find ("Camera"));
		//instantiate the game and camera from their prefabs
		Instantiate (gamePrefab,
			gameSpawn.position,
			gameSpawn.rotation);
		Instantiate (cameraPrefab,
			gameSpawn.position + new Vector3 (6f, -6f),
			gameSpawn.rotation);
		//find the outdoor task area
		OutdoorTaskController.outdoorTaskArea = GameObject.Find ("OutdoorTaskArea");
		//make task area invisible, shower alert, and outdoor value background invisible
		GameController.setImageColor (OutdoorTaskController.outdoorTaskArea, GameController.invisible);
		GameObject.Find ("OutdoorTaskValueBackground").GetComponent<Image> ().color = GameController.invisible;
		GameObject.Find ("showeralertbackground").GetComponent<Image> ().color = GameController.invisible;
		//set the task alert to inform the user
		GameObject.Find ("taskalertbackground").GetComponent<Image> ().color = Color.green;
		GameObject.Find ("taskalert").GetComponent<Text> ().text = "Collect coins";
		//set up outdoor task to begin after wait
		GameObject.Find ("GameController").GetComponent<OutdoorTaskController> ().enabled = true;
		//begin the global timer and allow the outdoor task to start
		//this also sets up clients to network their animator index
		gameCreated = true;
	}

	//this method handles networking of animators and is called one time at the beginning of update when the game is created
	private bool localInit ()
	{
		//if the game is created
		if (gameCreated) {
			//for clients
			if (!isServer) {
				//command the server to update the index
				CmdAnimUpdate (animIndex);
			}
			//this might be left over from pre-creator method spawning
			OutdoorTaskController.numTasksInitialized = 0;
			return true;
		} else
			return false;
	}

	// Update is called once per frame
	void Update ()
	{
		//only continue execution if this is local player
		if (!isLocalPlayer) {
			return;
		}
		updateCalls++;
		//assign animators one time
		if (!localInitialization) {
			localInitialization = localInit ();
		}
		/* This block allows early shower exit
		//test if pressing key down during shower before clothes change
		if (Input.GetKeyDown (KeyCode.B) && freezeTimer > 3) {
			//roll for infection
			int roll = Random.Range (0, 100);
			Debug.Log (roll);
			//if they were infected sync infection incurred across clients/host
			if (roll < GameController.infectionProbability) {
				if (isServer) {
					RpcInfectSync ();
				} else {
					CmdInfectSync ();
				}
			}
			//update shower text
			GameController.setImageColor ("showeralertbackground", Color.black);
			GameObject.Find ("showeralert").GetComponent<Text> ().text = "Early Exit";
			//update freeze timer
			freezeTimer = 4;
		}
		*/
		//get input from the player for movement
		Vector2 movementVector = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));
		//store gameplay data
		if (myGameplay.Last == null && gameCreated) {
			myGameplay.AddLast (new inputState (updateCalls, movementVector));
		}
		if (myGameplay.Last.Value.input != movementVector && gameCreated) {
			myGameplay.AddLast (new inputState (updateCalls, movementVector));
		};
		//if we have set the freezetimer to count down for the shower and it is over the threshold to force walk
		//or if we are hiding other player exit decision
		if (freezeTimer > 1 || GameObject.Find ("HidePlayerImage").GetComponent<Image> ().enabled == true) {
			//stop movement
			movementVector = new Vector2 (0, 0);
		}
		//now we will explicitly assign the movement vector based on parameter leftRight which flips for each entrance/exit
		else if (freezeTimer == 1) {
			movementVector = new Vector2 (leftRight, 0);
		}
		// loop to botify player movement
		//override player movement with previous round gameplay
		if(LobbyOverride.currentTreatment == 1 && GameController.globalTimer < 10f && gameCreated){
			if (tempMoveVect == null) {
				tempMoveVect = myGameplay.First.Value.input;
				myGameplay.RemoveFirst ();
			}
			if (myGameplay.First.Value.updateNumber == updateCalls ) {
				tempMoveVect = myGameplay.First.Value.input;
				myGameplay.RemoveFirst ();
			}
			movementVector = tempMoveVect;
		}
		//if player is walking update the animator
		if (movementVector != Vector2.zero) {
			anim.SetBool ("iswalking", true);
			anim.SetFloat ("input_x", movementVector.x);
			anim.SetFloat ("input_y", movementVector.y);
		}
		//if player is not walking
		else {
			anim.SetBool ("iswalking", false);
		}
		//move the player
		rigbod.MovePosition (rigbod.position + movementVector * Time.deltaTime * 3f);
		////////////////END MOVEMENT SECTION OF UPDATE/////////////////////////
		///THIS SECTION CONTROLS LOCAL UPDATES TO PLAYERS/////////////////////
		//if the task is instantiated
		if (OutdoorTaskController.taskInstantiated && !taskExitColorsChanged) {
			//if this player is allowed to exit enable exit and update text
			if (myNetId == GameController.currentPlayerTask) {
				//destroy coins for this players across all clients
				if (isServer) {
					RpcCoinDestroy ();
				} else {
					CmdCoinDestroy ();
				}
				//the colliders will no longer block exit
				GameObject.Find ("FireExit").GetComponent<BoxCollider2D> ().isTrigger = true;
				GameObject.Find ("RightShowerExit").GetComponent<BoxCollider2D> ().isTrigger = true;
				//update the task alert
				GameObject.Find ("taskalertbackground").GetComponent<Image> ().color = Color.red;
				GameObject.Find ("taskalert").GetComponent<Text> ().text = "Exit the facility";
				//enable interaction with fire exit collider
				GameObject.Find ("FireExit").GetComponentInChildren<CircleCollider2D> ().enabled = true;
				//open the right shower door
				RightShowerLerpScript.openingR = true;
				RightShowerLerpScript.closingR = false;
			} 
			//for the players not exiting hide the player who exited
			else {
				//hide normal task alert
				GameObject.Find ("taskalertbackground").GetComponent<Image> ().color = Color.grey;
				GameObject.Find ("taskalert").GetComponent<Text> ().text = "Wait for\nWorker " + GameController.currentPlayerTask;
				//for treatments without other player exit decision
				//if this is a treatment where we are hiding player exit decision
				if (LobbyOverride.treatmentArray [LobbyOverride.currentTreatment] == 1) {
					//hide the spite renderer to make the exiting player invisible
					foreach (GameObject p in GameController.players) {
						if (p.GetComponent<PlayerMovement> ().myNetId == GameController.currentPlayerTask) {
							p.GetComponent<SpriteRenderer> ().enabled = false;
						}
					}
					//hide the scoreboard
					GameObject.Find ("GUICanvas").GetComponent<Canvas> ().enabled = false;
					//old method to hide player exit, this is an old way that we no longer use
					//GameObject.Find ("HidePlayerImage").GetComponent<Image> ().enabled = true;
					//GameObject.Find ("HidePlayerText").GetComponent<Text> ().text = "Wait for\nWorker " + GameController.currentPlayerTask;
				} 
			}
			//do this one time
			taskExitColorsChanged = true;
		}
		//if a player enters the shower
		if (showerFreezeEnter) {
			//only execute this for that player
			if (myNetId == GameController.currentPlayerTask) {
				//initialize freezeTimer, this stops movement
				freezeTimer = GameController.showerTime;
				//inform user they are showering
				GameController.setImageColor ("showeralertbackground", GameController.showerColorArray [colorArrayIndex++ % GameController.showerColorArray.Length]);
				GameObject.Find ("showeralert").GetComponent<Text> ().text = "You Are Showering" + "\n" + freezeTimer + " seconds left";//this is commented out for early shower exit option + "\nPress b to exit early";
				//set up the routine for shower count down
				countDownRoutine = WaitAndDecrease (1f);
				//start the coroutine
				StartCoroutine (countDownRoutine);
				//decide direction of forced walk based on position and close the door behind
				if (GetComponent<Transform> ().position.x > 3.8) {
					leftRight = -1;
					RightShowerLerpScript.openingR = false;
					RightShowerLerpScript.closingR = true;
					RightShowerLerpScript.t = 0f;
				} else {
					leftRight = 1;
					LeftShowerLerp.openingR = false;
					LeftShowerLerp.closingR = true;
					LeftShowerLerp.t = 0f;
				}
			}
			//do this one time
			showerFreezeEnter = false;
		}
		//if the player did not exit and we need to seal the barn
		if (rpcNeeded) {
			//only execute this for that player
			if (myNetId == GameController.currentPlayerTask) {
				//the colliders will now block exit
				GameObject.Find ("FireExit").GetComponent<BoxCollider2D> ().isTrigger = false;
				GameObject.Find ("RightShowerExit").GetComponent<BoxCollider2D> ().isTrigger = false;
				//disable the fire exit collider which triggers door open and close
				GameObject.Find ("FireExit").GetComponentInChildren<CircleCollider2D> ().enabled = false;
				//reset the interpolators
				RightShowerLerpScript.t = 0f;
				FireExitLerper.t = 0f;
				//close the right shower door
				RightShowerLerpScript.openingR = false;
				RightShowerLerpScript.closingR = true;
				//close the fire exit 
				FireExitLerper.closingR = true;
				FireExitLerper.openingR = false;
				//if this player is the host set up a new task
				//if player is a client command the server to set up the task
				if (isServer) {
					RpcTaskInstantiate ();
				} else {
					CmdTaskInstantiate ();
				}
			}
			//do this one time
			rpcNeeded = false;
		}
		//this method collects score locally and loads the round end scene
		if (endDayUpdate) {
			//if we are decreasing score for infection
			bool infectionDecrement = false;
			//if any players were infected
			foreach (GameObject p in GameController.players) {
				if (p.GetComponent<PlayerMovement> ().infectionIncurred) {
					infectionDecrement = true;
				}
			}
			//decrease the score one time
			if (infectionDecrement) {
				playerScore -= GameController.infectionCost;
				//update the text for infection
				if (isLocalPlayer) {
					//update session score
					foreach (GameObject lobPl in GameObject.FindGameObjectsWithTag("LobbyPlayer")) {
						if (lobPl.GetComponent<LobbyPlayerStats> ().myNetId == myNetId) {
							lobPl.GetComponent<LobbyPlayerStats> ().sessionScore -= GameController.infectionCost;
							lobPl.GetComponentInChildren<Text> ().text = "MyNetId: " + myNetId + " Session Score: " + lobPl.GetComponent<LobbyPlayerStats> ().sessionScore + "\nYour facility was infected";
						}
					}
				}
			}
			//do this one time
			endDayUpdate = false;
			//set up the player to load the scene after a wait
			loadLevelRoutine = nextRoundBegin ("Lobby Scene");
			StartCoroutine (loadLevelRoutine);
		}
		//this block resets the text for the player who exited
		if (reenterText) {
			GameObject.Find ("taskalertbackground").GetComponent<Image> ().color = Color.green;
			GameObject.Find ("taskalert").GetComponent<Text> ().text = "Collect coins";
			reenterText = false;
		}
	}
	//when a local trigger enters the player's collider
	void OnTriggerEnter2D (Collider2D other)
	{
		//based on the name of the other collider
		switch (other.name) {
		//if entering the barn
		case ("BarnArea"):
			inBarn = true;
			break;
			//if entering outdoor task area
		case("OutdoorTaskArea"):
			//define the routine to count up score while on outdoor task area
			countUpRoutine = WaitAndIncrease (1f, GameController.outdoorTaskValuePerSecond);
			StartCoroutine (countUpRoutine);
			break;
		case("ShowerFreezeArea"):
			//update wil enact this locally
			foreach (GameObject p in GameController.players) {
				p.GetComponent<PlayerMovement> ().showerFreezeEnter = true;
			}
			break;
		case("USD(Clone)"):
			//only allow local player to destroy coin and increase score based on tag
			if (other.tag == myNetId.ToString ()) {
				//increment score
				playerScore += GameController.coinValue;
				if (isLocalPlayer) {
					//update session score
					foreach (GameObject lobPl in GameObject.FindGameObjectsWithTag("LobbyPlayer")) {
						if (lobPl.GetComponent<LobbyPlayerStats> ().myNetId == myNetId) {
							lobPl.GetComponent<LobbyPlayerStats> ().sessionScore += GameController.coinValue;
							lobPl.GetComponentInChildren<Text> ().text = "MyNetId: " + myNetId + " Session Score: " + lobPl.GetComponent<LobbyPlayerStats> ().sessionScore;
						}
					}
				}
				//update worldspace and screen overlay canvas
				scoreFlashRoutine = scoreFlash (.5f, "+$" + GameController.coinValue, this.transform.GetChild (0).gameObject);
				//update the score using the player who is the host call the rpc score update method
				if (isServer) {
					RpcScoreUpdate (myNetId, playerScore.ToString ());
				}
				//start the coroutine to flash the score
				StartCoroutine (scoreFlashRoutine);
				//destroy the object and decrement num coins spawned
				Destroy ((GameObject)other.gameObject);
				numCoinsSpawned--;
			}
			break;
			//only lock down barn if actually reentering, this will stop a false seal on attempted exit
		case("RightShowerExit"):
			if (!inBarn) {
				allowClosal = true;
			}
			break;
			//only lock down barn if actually reentering, this will stop a false seal on attempted exit
		case("FireExit"):
			if (!inBarn) {
				allowClosal = true;
			}
			break;
		}
	}
	//when a local trigger exits the player collider
	void OnTriggerExit2D (Collider2D other)
	{
		//based on the name
		switch (other.name) {
		//if exiting the barn
		case("BarnArea"):
			inBarn = false;
			break;
			//if exiting outdoor task area
		case("OutdoorTaskArea"):
			StopCoroutine (countUpRoutine);
			break;
		case("FireExit"):
			//if exiting the barn
			if (!inBarn) {
				//sync name change color
				if (isServer) {
					RpcColorChange (this.gameObject, Color.yellow, true);
				} else {
					CmdColorChange (this.gameObject, Color.yellow, true);
				}
				//reset the interpolator
				RightShowerLerpScript.t = 0f;
				//close the right shower door
				RightShowerLerpScript.openingR = false;
				RightShowerLerpScript.closingR = true;
				//roll for infection
				int roll = Random.Range (0, 100);
				Debug.Log (roll);
				//if they were infected sync infection incurred across clients/host
				if (roll < GameController.infectionProbability) {
					if (isServer) {
						RpcInfectSync ();
					} else {
						CmdInfectSync ();
					}
				}
				//if entering the barn
			} else {
				//dont allow non tasked player to trigger coin respawning
				if (myNetId == GameController.currentPlayerTask) {
					//if we are allowing closal seal the barn
					if (allowClosal) {
						//make the actual fire exit  and right shower entrance impenetrable
						GameObject.Find ("FireExit").GetComponent<BoxCollider2D> ().isTrigger = false;
						GameObject.Find ("RightShowerExit").GetComponent<BoxCollider2D> ().isTrigger = false;
						//disable the fire exit collider to trigger door open and close
						GameObject.Find ("FireExit").GetComponentInChildren<CircleCollider2D> ().enabled = false;
						//if this player is the host set up a new task
						//if player is a client command the server to set up the task
						if (isServer) {
							RpcTaskInstantiate ();
						} else {
							CmdTaskInstantiate ();
						}
						//reset allow closal
						allowClosal = false;
					}
				}
			}
			break;
			//if we are allowing closal and they are in the barn seal the barn
		case("RightShowerExit"):
			if (inBarn && allowClosal) {
				completeBarnClosal ();
				allowClosal = false;
			}
			break;
		}
	}
	//this method seals the barn on right shower exit inside
	private void completeBarnClosal ()
	{
		//dont allow non tasked player to trigger coin respawning
		if (myNetId == GameController.currentPlayerTask) {
			//seal the entrances
			GameObject.Find ("FireExit").GetComponent<BoxCollider2D> ().isTrigger = false;
			GameObject.Find ("RightShowerExit").GetComponent<BoxCollider2D> ().isTrigger = false;
			//disable the fire exit collider to trigger door open and close
			GameObject.Find ("FireExit").GetComponentInChildren<CircleCollider2D> ().enabled = false;
			//reset the interpolators
			RightShowerLerpScript.t = 0f;
			FireExitLerper.t = 0f;
			//close the right shower door
			RightShowerLerpScript.openingR = false;
			RightShowerLerpScript.closingR = true;
			//close the fire exit 
			FireExitLerper.closingR = true;
			FireExitLerper.openingR = false;
			//if this player is the host set up a new task
			//if player is a client command the server to set up the task
			if (isServer) {
				RpcTaskInstantiate ();
			} else {
				CmdTaskInstantiate ();
			}
		}
	}
	//increase player score based on parameters waitTime and decrementValue
	private IEnumerator WaitAndIncrease (float waitTime, float decrementValue)
	{
		while (true) {
			yield return new WaitForSeconds (waitTime);
			//if the value given to the player is less than available update decrement value
			if ((GameController.outdoorTaskValue - decrementValue) < 0) {
				decrementValue = GameController.outdoorTaskValue;
			}
			//increment score
			playerScore += decrementValue;
			if (isLocalPlayer) {
				//update session score
				foreach (GameObject lobPl in GameObject.FindGameObjectsWithTag("LobbyPlayer")) {
					if (lobPl.GetComponent<LobbyPlayerStats> ().myNetId == myNetId) {
						lobPl.GetComponent<LobbyPlayerStats> ().sessionScore += GameController.coinValue;
						lobPl.GetComponentInChildren<Text> ().text = "MyNetId: " + myNetId + " Session Score: " + lobPl.GetComponent<LobbyPlayerStats> ().sessionScore;
					}
				}
			}
			//update score
			if (isServer) {
				RpcScoreUpdate (myNetId, playerScore.ToString ());
			}
			//update worldspace and screen overlay canvas
			scoreFlashRoutine = scoreFlash (.5f, "+$" + decrementValue, this.transform.GetChild (0).gameObject);
			StartCoroutine (scoreFlashRoutine);
		}
	}
	//handle shower countdown
	private IEnumerator WaitAndDecrease (float waitTime)
	{
		while (true) {
			yield return new WaitForSeconds (waitTime);
			//decrement local timer
			freezeTimer--;
			//if time is up
			if (freezeTimer == 0) {
				//make shower info dissapear
				GameController.setImageColor ("showeralertbackground", GameController.invisible);
				GameObject.Find ("showeralert").GetComponent<Text> ().text = "";
				StopCoroutine (countDownRoutine);
			} else {
				//halfway through shower change animator for different clothes and close door behind
				if (freezeTimer == 3) {
					if (leftRight == 1) {
						animIndex++;
						RightShowerLerpScript.t = 0f;
						RightShowerLerpScript.openingR = true;
						RightShowerLerpScript.closingR = false;
					} else {
						animIndex--;
						LeftShowerLerp.openingR = true;
						LeftShowerLerp.closingR = false;
						LeftShowerLerp.t = 0f;
					}
					//update the animator for all players
					if (isServer) {
						RpcAnimUpdate (animIndex);
					} else {
						CmdAnimUpdate (animIndex);
					}

				}
				//change the color and inform the user they are showering
				GameController.setImageColor ("showeralertbackground", GameController.showerColorArray [(colorArrayIndex++) % GameController.showerColorArray.Length]);
				//this is commented out for early shower exit option
				//if (freezeTimer > 3) {
				//	GameObject.Find ("showeralert").GetComponent<Text> ().text = "You Are Showering" + "\n" + freezeTimer + " seconds left" + "\nPress b to exit early";
				//} else {
				GameObject.Find ("showeralert").GetComponent<Text> ().text = "You Are Showering" + "\n" + freezeTimer + " seconds left";
				//}
			}
		}
	}
	//this method spawns new tasks, ienumerator means it can pause itself
	private IEnumerator SpawnTasks (int myNetId, Color myColor)
	{
		while (true) {
			//if there is not a task occurring, commented out because we are now letting coins spawn for not tasked players
			//if (!OutdoorTaskController.taskInstantiated) {
			//if there are less than the maxCoinsInstantiated currently spawned
			if (numCoinsSpawned < GameController.maxCoinsInstantiated) {
				//instantiate the task with no spin
				GameObject localCoin = Instantiate (coin, spawnPositions [spawnIndex++ % spawnPositions.Length], Quaternion.identity);
				//set the tag and color to differentiate by player
				localCoin.tag = myNetId.ToString ();
				localCoin.GetComponent<SpriteRenderer> ().color = myColor;
				//increment currentTasks
				numCoinsSpawned++;
			}
			//}
			//break in between tasks
			yield return new WaitForSeconds (GameController.taskWait);
		}
	}
	//this method flashes the score for a specified amount of time
	private IEnumerator scoreFlash (float flashlength, string text, GameObject canvas)
	{
		//turn on the text and image canvas
		canvas.GetComponentInChildren<Text> ().text = text;
		canvas.GetComponentInChildren<Image> ().enabled = true;
		//wait for flashlength
		yield return new WaitForSeconds (flashlength);
		//remove text and image, end coroutine
		canvas.GetComponentInChildren<Text> ().text = "";
		canvas.GetComponentInChildren<Image> ().enabled = false;
		StopCoroutine (scoreFlashRoutine);
	}
	//this method starts the next round
	private IEnumerator nextRoundBegin (string name)
	{
		yield return new WaitForSeconds (0);
		if (isServer) {
			NetworkManager.singleton.ServerChangeScene (name);
		}
		StopCoroutine (loadLevelRoutine);
		StopCoroutine (taskSpawnRoutine);
	}
	//wait to reveal player after reentry
	private IEnumerator reentryUnhide ()
	{
		yield return new WaitForSeconds (3);
		//make the player visible again
		foreach (GameObject p in GameController.players) {
			p.GetComponent<SpriteRenderer> ().enabled = true;
		}
		//for treatments without other player exit decision
		//GameObject.Find ("HidePlayerImage").GetComponent<Image> ().enabled = false;
		//make the scoreboard visible
		GameObject.Find ("GUICanvas").GetComponent<Canvas> ().enabled = true;
		//GameObject.Find ("HidePlayerText").GetComponent<Text> ().text = "";
		StopCoroutine (unHideRoutine);
	}
	//for host to client score updates
	[ClientRpc]
	public void RpcScoreUpdate (int idToIncrement, string score)
	{
		GameObject.Find ("Player" + idToIncrement + "Score").GetComponent<Text> ().text = "$" + score;
	}
	//for client score updates
	[Command]
	public void CmdScoreUpdate (int idToIncrement, string score)
	{
		RpcScoreUpdate (idToIncrement, score);
	}
	//set up a new task for all clients
	[ClientRpc]
	public void RpcTaskInstantiate ()
	{
		//set up outdoor task to begin after wait
		OutdoorTaskController.needNewTask = true;
		//find the player who was just tasked and set their coins to spawn again
		foreach (GameObject p in GameController.players) {
			if (p.GetComponent<PlayerMovement> ().myNetId == GameController.currentPlayerTask) {
				p.GetComponent<PlayerMovement> ().numCoinsSpawned = 0;
			}
		}
		//allow a different player to exit
		//this line was commented out because player task synchornization was achieved
		//foreach (GameObject player in GameController.players) {
		GameController.currentPlayerTask = (GameController.currentPlayerTask % GameController.totalPlayers) + 1;
		//}
		//if this is a treatment where we are hiding player exit decision
		if (LobbyOverride.treatmentArray [LobbyOverride.currentTreatment] == 1) {
			unHideRoutine = reentryUnhide ();
			StartCoroutine (unHideRoutine);
		}
		//for treatments without other player exit decision
		//GameObject.Find ("HidePlayerImage").GetComponent<Image> ().enabled = false;
		//GameObject.Find ("HidePlayerText").GetComponent<Text> ().text = "";
		//normal task alert
		GameObject.Find ("taskalertbackground").GetComponent<Image> ().color = Color.green;
		GameObject.Find ("taskalert").GetComponent<Text> ().text = "Collect coins";
	}
	//command the server to instante a new task
	[Command]
	public void CmdTaskInstantiate ()
	{
		RpcTaskInstantiate ();
	}
	//change the color of a player's name host to client
	[ClientRpc]
	public void RpcColorChange (GameObject obj, Color color, bool state)
	{
		obj.transform.GetChild (1).GetComponentInChildren<Text> ().color = color;
	}
	//command the server to change the player's name
	[Command]
	public void CmdColorChange (GameObject obj, Color color, bool state)
	{
		RpcColorChange (obj, color, state);
	}
	//destroy coins across clients
	[ClientRpc]
	public void RpcCoinDestroy ()
	{
		//find coins based on all tags of players and destroy
		//for (int i = 1; i < GameController.totalPlayers + 1; i++) {
		foreach (GameObject g in GameObject.FindGameObjectsWithTag(GameController.currentPlayerTask.ToString())) {
			Destroy (g);
		}
		//	}
	}
	//command the server to destroy all coins
	[Command]
	public void CmdCoinDestroy ()
	{
		RpcCoinDestroy ();
	}
	//update the runtime animator for a player across clients
	[ClientRpc]
	public void RpcAnimUpdate (int index)
	{
		//assign animation controller
		anim.runtimeAnimatorController = Resources.Load (animatorPaths [index]) as RuntimeAnimatorController;
	}
	//command the server to update the animator
	[Command]
	public void CmdAnimUpdate (int index)
	{
		RpcAnimUpdate (index);
	}
	//sync infection incurred accross client
	[ClientRpc]
	public void RpcInfectSync ()
	{
		infectionIncurred = true;
	}
	//command the server to sync infection incurred
	[Command]
	public void CmdInfectSync ()
	{
		RpcInfectSync ();
	}

	public class inputState
	{
		public int updateNumber;
		public Vector2 input;

		public inputState (int currentUpdate, Vector2 movementState)
		{
			this.updateNumber = currentUpdate;
			this.input = movementState;
		}
	}
}