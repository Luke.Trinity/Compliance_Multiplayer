﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FireExitLerper : MonoBehaviour
{
	//booleans to trigger an open or close of this door
	public static bool openingR = false;
	public static bool closingR = false;
	//range of values to interpolate
	private float minimum = 0f;
	private float maximum =  1f;
	// starting value for the Lerp interpolater
	public static float t = 0.0f;
	void Update ()
	{		
		//if opening or closing the door set the size delta y value of the rect transform using the lerp function
		if (openingR) {
			GameObject.Find("FireExitImage").GetComponent<RectTransform> ().sizeDelta = new Vector2 (.07f, Mathf.Lerp (GameObject.Find("FireExitImage").GetComponent<RectTransform> ().sizeDelta.y, minimum, t));
			//increate the t interpolater
			t += 0.5f * Time.deltaTime;
			if (GameObject.Find("FireExitImage").GetComponent<RectTransform> ().sizeDelta == new Vector2 (.07f, minimum)) {
				t = 0f;
				openingR = false;
			}
		} else if(closingR) {
			GameObject.Find("FireExitImage").GetComponent<RectTransform> ().sizeDelta = new Vector2 (.07f, Mathf.Lerp (GameObject.Find("FireExitImage").GetComponent<RectTransform> ().sizeDelta.y, maximum, t));
			// .. and increate the t interpolater
			t += 0.5f * Time.deltaTime;
			if (GameObject.Find("FireExitImage").GetComponent<RectTransform> ().sizeDelta == new Vector2 (.07f, maximum)) {
				t = 0f;
				closingR = false;
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		//if the player currently allowed to complete a task enters the collider open the door
		if (other.GetComponent<PlayerMovement> ().myNetId == GameController.currentPlayerTask) {
			openingR = true;
			closingR = false;
			t = 0f;
		}
	}
	void OnTriggerExit2D(Collider2D other){
		//if the player currently allowed to complete a task exits the collider close the door
		if (other.GetComponent<PlayerMovement> ().myNetId == GameController.currentPlayerTask) {
			closingR = true;
			openingR = false;
			t = 0f;
		}
	}
}
