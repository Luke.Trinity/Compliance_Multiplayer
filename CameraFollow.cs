﻿using UnityEngine;
using System.Collections;

//this script controls the camera
public class CameraFollow : MonoBehaviour
{
	//location of target
	//assign in localplayer start
	public static Transform target;
	//camera variable
	public Camera myCam;

	// initialization
	void Start ()
	{
		//get information from Camera class
		myCam = GetComponent<Camera> ();
		//can only have one audio listener, each player has a camera attached
		myCam.GetComponent<AudioListener> ().enabled = false;
	}

	void Update ()
	{
		//make the screen reziable , decrease last number show more of game
		myCam.orthographicSize = (Screen.height / 95f) / 2f;
		//if target exists
		if (target) {
			//stop camera at edge of game, limits for where the camera can move
			float camX = Mathf.Clamp (target.position.x, 5.5f, 6.4f);
			float camY = Mathf.Clamp (target.position.y, -7.4f, -4.5f);
			//generate smooth movement towards player using linear interpolation, -10 to keep player in view
			transform.position = Vector3.Lerp (transform.position, new Vector3 (camX, camY, target.position.z), 0.1f) + new Vector3 (0, 0, -10);
		}

	}
}
