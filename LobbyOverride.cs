﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LobbyOverride : NetworkLobbyManager {
	//create a treatment array for testing player hide
	public static int[] treatmentArray = new int[] {1,2};
	//iterator for treatmentArray
	public static int currentTreatment = 0;
	//how many plaers the game is set up for
	public static int totalPlayers = 1;
	//routine to pause before next round
	private IEnumerator waitToStartRoutine;

	// for users to apply settings from their lobby player object to their in-game player object
	public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
	{
		//assign game player net id
		gamePlayer.GetComponent<PlayerMovement> ().myNetId = lobbyPlayer.GetComponent<LobbyPlayerStats> ().myNetId;
		return true;
	}
	/*
	public override void OnClientSceneChanged (NetworkConnection conn)
	{
		base.OnClientSceneChanged (conn);
		//if this is not the main scene
		if (SceneManager.GetActiveScene ().name != "Main") {
			//hide each players lobby gui and show their canvases
			foreach (NetworkLobbyPlayer lobbyPlayer in GameObject.FindObjectsOfType<NetworkLobbyPlayer>()) {
				lobbyPlayer.GetComponent<NetworkLobbyPlayer> ().ShowLobbyGUI = false;
				lobbyPlayer.GetComponentInChildren<Canvas> ().enabled = true;
			};
			//hide lobby manager gui
			showLobbyGUI = false;
			//define the coroutine to wait before the next round
			waitToStartRoutine = Wait ();
			//start the routine
			StartCoroutine (waitToStartRoutine);
		}
	}
	//this method waits to start next round
	private IEnumerator Wait ()
	{
		yield return new WaitForSeconds (5);
		//for each player hide the canvas
		foreach (NetworkLobbyPlayer lobbyPlayer in GameObject.FindObjectsOfType<NetworkLobbyPlayer>()) {
			lobbyPlayer.GetComponentInChildren<Canvas> ().enabled = false;
			//send the message that players are ready so the next round starts
			if (lobbyPlayer.GetComponent<LobbyPlayerStats> ().myNetId != 1 && SceneManager.GetActiveScene().name != "Main") {
				lobbyPlayer.GetComponent<NetworkLobbyPlayer> ().SendReadyToBeginMessage ();
			}
		}
		StopCoroutine (waitToStartRoutine);
	}
*/
}
