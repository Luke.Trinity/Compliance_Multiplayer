﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
	//global clock
	public static float globalTimer;
	//number of players
	public static int totalPlayers;
	// wait before outdoorTask is instantiated after last player spawns and between tasks
	public static int outdoorTaskWait;
	//outdoor task value
	public static float outdoorTaskValue;
	//outdoor task value
	public static float OUTDOORVALUE;
	//how much a player gets for each second on the outdoor task
	public static float outdoorTaskValuePerSecond;
	//constant for vps
	public static float OUTDOORTASKVALUEPERSECOND;
	//completely transparent color
	public static Color invisible = new Color (0, 0, 0, 0);
	//array of shower colors
	public static Color[] showerColorArray;
	//showerTimer
	public static int showerTime;
	//array of all players
	public static GameObject[] players;
	//most coins allowed at any one time per player
	public static int maxCoinsInstantiated;
	//time in between coins spawning
	public static float taskWait;
	//value of a coin
	public static float coinValue;
	//if we are stopping alerts due to infection *NOT IMPLEMENTED YET*
	public static bool disableTaskAlertOnInfection = false;
	//how many tasks do we want
	public static int totalTasks;
	//chances to get an infection on exit of fire door
	public static int infectionProbability;
	//cost of an infection
	public static int infectionCost;
	//game time
	public float gameTime;
	//if all players face penalty for infection
	public static bool infectionPenalty;
	//only allow one net id increment
	public static bool indexUp;
	//which player will be able to exit for the outdoor task
	public static int currentPlayerTask;
	//track number of player start method initializations
	public static int playerInitializations;

	// Use this for initialization
	void Start ()
	{
		//no players have initialized yet
		playerInitializations = 0;
		//which player can exit first
		currentPlayerTask = 1;
		//this will always start at 0
		globalTimer = 0f;
		//all players are penalized for infection if this is true
		infectionPenalty = true;
		//how long is the game
		gameTime = 10f;
		//keep the total players in lobby override for organization (database variable consolidation)
		totalPlayers = LobbyOverride.totalPlayers;
		//how many tasks will occur this round
		totalTasks = 0;
		//how long before the outdoor task starts and in between tasks
		outdoorTaskWait = 10;
		//how much is the outdoor task worth
		OUTDOORVALUE = 15;
		//how quickly does the outdoor task decrease in value
		OUTDOORTASKVALUEPERSECOND = 1f;
		//only allow one coin to instantiate at a time
		maxCoinsInstantiated = 1;
		//each coin is worth 1
		coinValue = 1f;
		//time in between coin spawning, nearly instantaneous
		taskWait = .2f;
		//how likely is it that an infection occurs on fire exit
		infectionProbability = 100;
		//what is the cost of an infection
		infectionCost = 20;
		//how long does it take to shower
		showerTime = 8;
		//colors for shower countdown
		showerColorArray = new Color[] {
			new Color32 (255, 71, 26, 255),
			new Color32 (255, 153, 51, 255),
			new Color32 (255, 204, 102, 255),
			new Color32 (102, 204, 0, 255),
			new Color32 (0, 204, 0, 255)
		};
		//set task value variables which can be modified to be equal to the constants which will not change
		outdoorTaskValue = OUTDOORVALUE;
		outdoorTaskValuePerSecond = OUTDOORTASKVALUEPERSECOND;
	}
	// Update is called once per frame
	void Update ()
	{
		//increment time if the game is created
		if (PlayerMovement.gameCreated) {
			//increment game timer
			globalTimer += Time.deltaTime;
		}
		//end game call to players
		if (globalTimer > gameTime) {
			//increment the treatment array iterator
			LobbyOverride.currentTreatment++;
			//do this once by setting game time to be essentially infinity
			gameTime = 1000000f;
			//for each player
			foreach(GameObject player in GameController.players){
				//avoid null reference exception
				if (player.gameObject != null) {
						//end the game
						player.GetComponent<PlayerMovement> ().endDayUpdate = true;
				}
			}
		}
	}
	//these methods set the color of an object with an image component in its child by an input object or it's name
	public static void setImageColor (string name, Color colorInput)
	{
		GameObject inputObj = GameObject.Find (name);
		inputObj.GetComponentInChildren<Image> ().color = colorInput;
	}
	public static void setImageColor (GameObject inputObj, Color colorInput)
	{
		inputObj.GetComponentInChildren<Image> ().color = colorInput;
	}

	//method to test if a timer is in a range
	public static bool between (float timer, float min, float window)
	{
		if (timer > min && timer < min + window) {
			return true;
		} else {
			return false;
		}
	}
	//method to set colliders' triggers by string input array
	public static void setTriggers (string[] names, bool state)
	{
		foreach (string name in names) {
			GameObject.Find (name).GetComponent<BoxCollider2D> ().isTrigger = state;
		}
	}
	//make a group ob game objects visible or invisible
	//parameters: tag of the object holding the group of objects and tf, the boolean value of true or false
	public static void SetChildRenderers(string tag, bool tf)
	{
		GameObject[] objects = GameObject.FindGameObjectsWithTag (tag);
			for (int k = 0; k < objects.Length; k++) {
				SpriteRenderer[] renderers = objects [k].GetComponentsInChildren<SpriteRenderer> ();
				for (int i = 0; i < renderers.Length; i++) {
					renderers [i].enabled = tf;
				}
			}
	}
}