﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OutdoorTaskController : MonoBehaviour
{
	//rotuine to wait before starting outdoor task
	private IEnumerator outdoorTaskInitializationRoutine;
	//task decrease ienumerator
	private IEnumerator outdoorTaskCountDown;
	//count number of tasks
	public static int numTasksInitialized;
	//bool to check for task end
	public static bool checkForTaskEnd = true;
	public static bool needNewTask = true;
	//whether task is instantiated
	public static bool taskInstantiated = false;
	//outdoor task object
	public static GameObject outdoorTaskArea;

	void Update ()
	{
		//if we need a new task, one is not happening, and the game is created
		if (needNewTask && !taskInstantiated && PlayerMovement.gameCreated) {
			//if we have not initialized our total
			if (OutdoorTaskController.numTasksInitialized < GameController.totalTasks) {
				//initialize the task wait routine
				outdoorTaskInitializationRoutine = WaitToStart ();
				StartCoroutine (outdoorTaskInitializationRoutine);
				numTasksInitialized++;
				needNewTask = false;
			}
		}
	}
	//wait and then initialize the task
	private IEnumerator WaitToStart ()
	{
		yield return new WaitForSeconds (GameController.outdoorTaskWait);
		//set up local player to allow exit and stop coin spawning
		foreach (GameObject p in GameController.players) {
			p.GetComponent<PlayerMovement> ().taskExitColorsChanged = false;
		}
		//reset outdoorTaskValue
		GameController.outdoorTaskValue = GameController.OUTDOORVALUE;
		//make the outdoor task area visible
		GameController.setImageColor (outdoorTaskArea, Color.red);
		//update the text
		GameObject.Find ("OutdoorTaskCanvas").GetComponentInChildren<Text> ().text = "$" + GameController.outdoorTaskValue.ToString ();
		GameObject.Find ("OutdoorTaskValueBackground").GetComponent<Image> ().color = Color.blue;
		//turn on the outdoor task collider
		outdoorTaskArea.GetComponentInChildren<BoxCollider2D> ().enabled = true;
		//task is currently occuring
		taskInstantiated = true;
		//start the countdown routine
		outdoorTaskCountDown = WaitAndDecrease (1);
		StartCoroutine (outdoorTaskCountDown);
		//stop the coroutine
		StopCoroutine (outdoorTaskInitializationRoutine);
	}
	//method to decrease the outdoor task value
	private IEnumerator WaitAndDecrease (int decreaseWait)
	{
		while (true) {
			//if the task is no longer instantiated end the coroutine
			if (!taskInstantiated) {
				StopCoroutine (outdoorTaskCountDown);
			}
			//update the text
			yield return new WaitForSeconds (decreaseWait);
			//if the value per second is greater than available value
			if (GameController.outdoorTaskValue - GameController.outdoorTaskValuePerSecond < 0) {
				//update value per second
				GameController.outdoorTaskValuePerSecond = GameController.outdoorTaskValue;
			}
			//decrement
			GameController.outdoorTaskValue -= GameController.outdoorTaskValuePerSecond;
			//reset the task value for next task
			GameController.outdoorTaskValuePerSecond = GameController.OUTDOORTASKVALUEPERSECOND;
			//update the text
			GameObject.Find ("OutdoorTaskCanvas").GetComponentInChildren<Text> ().text = "$" + GameController.outdoorTaskValue.ToString ();
			//if the task has run out of value
			if (GameController.outdoorTaskValue <= 0) {
				//update the text
				GameObject.Find ("OutdoorTaskCanvas").GetComponentInChildren<Text> ().text = "";
				//make the outdoor task area invisible
				GameController.setImageColor (outdoorTaskArea, GameController.invisible);
				GameObject.Find ("OutdoorTaskValueBackground").GetComponent<Image> ().color = GameController.invisible;
				//turn off the outdoor task collider
				GetComponentInChildren<BoxCollider2D> ().enabled = false;
				//task is no longer instantiated
				taskInstantiated = false;
				foreach (GameObject p in GameController.players) {
					//set up local text reset for player who exited
					if(GameController.currentPlayerTask == p.GetComponent<PlayerMovement>().myNetId){
					p.GetComponent<PlayerMovement> ().reenterText = true;
					}
					//if the player did not exit the barn for the task
					if (p.GetComponent<PlayerMovement> ().inBarn == true && GameController.currentPlayerTask == p.GetComponent<PlayerMovement>().myNetId) {
						//seal the barn and instantiate a task
						p.GetComponent<PlayerMovement>().rpcNeeded = true;
					}
				}
			}
		}
	}
}
